package com.kakaruu.practice.batch.jobs;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.job.flow.FlowExecutionStatus;
import org.springframework.batch.core.job.flow.JobExecutionDecider;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Random;

@Configuration
@Slf4j
@RequiredArgsConstructor
public class conditionalFlowJobConfig {

    private final JobBuilderFactory jobBuilderFactory; // Job을 쉽게 만들 수 있도록 해주는 클래스
    private final StepBuilderFactory stepBuilderFactory;

    @Bean
    public Job conditionalFlowJob() {
        return jobBuilderFactory.get("stepNextConditionalJob")
                .start(conditionalFlowStep1()) // step1에서 시작했을 때
                .next(conditionalFlowDecider()) // decider의 결과를 받아온다.
                    .on("ZERO") // decider의 결과가 ZERO인 경우 Flow 설정
                    .to(conditionalFlowStep3()) // ZERO인 경우 step3으로 이동한다.
//                    .on("*") // step3에서 어떤 결과가 나오건
//                    .end() // Flow 종료
                .from(conditionalFlowDecider()) // decider에 대해 추가 설정
                    .on("ONE") // decider의 결과가 ONE인 경우 Flow 설정
                    .to(conditionalFlowStep2()) // ONE인 경우 step2로 이동한다.
//                    .on("*") // step2에서 어떤 결과가 나오건
//                    .end() // Flow 종료
                .next(conditionalFlowStep3()) // 앞에서 step3이 이미 실행되었으면 무시됨
                .next(conditionalFlowStep4())
                .end() // Job 종료
                .build();
    }

    @Bean
    public Step conditionalFlowStep1() {
        return stepBuilderFactory.get("conditionalFlowStep1")
                .tasklet((contribution, chunkContext) -> {
                    log.warn("conditionalFlowStep1 시작");

//                    // Step의 Flow는 ExitStatus에 따라 결정된다.
//                    contribution.setExitStatus(ExitStatus.FAILED);

                    // Step의 Flow는 RepeatStatus랑은 관련이 없다.
                    return RepeatStatus.FINISHED;
                })
                .build();
    }

    @Bean
    public Step conditionalFlowStep2() {
        return stepBuilderFactory.get("conditionalFlowStep2")
                .tasklet((contribution, chunkContext) -> {
                    log.warn("conditionalFlowStep2 시작");
                    return RepeatStatus.FINISHED;
                })
                .build();
    }

    @Bean
    public Step conditionalFlowStep3() {
        return stepBuilderFactory.get("conditionalFlowStep3")
                .tasklet((contribution, chunkContext) -> {
                    log.warn("conditionalFlowStep3 시작");
                    return RepeatStatus.FINISHED;
                })
                .build();
    }

    @Bean
    public Step conditionalFlowStep4() {
        return stepBuilderFactory.get("conditionalFlowStep4")
                .tasklet((contribution, chunkContext) -> {
                    log.warn("conditionalFlowStep4 시작");
                    return RepeatStatus.FINISHED;
                })
                .build();
    }

    @Bean
    public JobExecutionDecider conditionalFlowDecider() {
        return new FlowDecider();
    }

    public static class FlowDecider implements JobExecutionDecider {

        @Override
        public FlowExecutionStatus decide(JobExecution jobExecution, StepExecution stepExecution) {
            Random rand = new Random();
            int randomNumber = rand.nextInt(2);
            log.warn("randomNumber: {}", randomNumber);

            if(randomNumber == 0) {
                return new FlowExecutionStatus("ZERO");
            } else {
                return new FlowExecutionStatus("ONE");
            }
        }
    }
}
