package com.kakaruu.practice.batch.jobs;

import com.kakaruu.practice.batch.tasklets.TutorialTasklet;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
@Slf4j
public class TutorialConfig {

    private final JobBuilderFactory jobBuilderFactory; // Job을 쉽게 만들 수 있도록 해주는 클래스
    private final StepBuilderFactory stepBuilderFactory;

    // Job은 전체 배치 프로세스를 캡슐화하는 엔티티로, Step 들을 담는 컨테이너라고 할 수 있다.
    // Step 인스턴스들의 순서를 정의하고, 작업의 재시작 여부 등을 설정할 수 있다.
    @Bean
    public Job tutorialJob() {
        // JobScope일때 tutorialStep이 프록시인지.. 확인좀
        // => 프록시 맞다. JobScope 제거하면 프록시 해제 됨
        Step step1 = tutorialStep(null, null, null, null);

        return jobBuilderFactory.get("tutorialJob") // Job의 이름 설정
                .start(step1) // job이 처음으로 시작할 스텝
//                .next(step2) // 그 다음 실행할 스텝은 next로 설정한다.
                .build();

        // # JobInstance
        // Job이 실행된 상태를 JobInstance라고 한다.
        // 프로그램과 프로세스의 관계와 비슷하다.

        // 예를 들어 하루에 한번씩 실행되는 Job이 있다면 매일 JobInstance가 생성된다.
        // 하지만 만약 이전 JobInstance가 실패한 경우 다음날은 새 Instance를 생성하지 않고 실패한 JobInstance를 사용한다.
        // BATCH_JOB_INSTANCE 테이블에서 확인할 수 있다.

        // # jobExecution
        // 단, 실패한 jobInstance를 재사용하는 경우, 각각 실행하는 jobExecution은 다르다.
        // BATCH_JOB_EXECUTION 테이블에서 확인할 수 있다.

        // # JobExecutionContext
        // Job 실행 중 개발자가 지속적으로 유지되었으면 하는 데이터를 저장하는 저장소를 의미한다.
        // 이 저장소는 프레임워크에 의해 유지 및 관리되고 Job이 실행되는 중간 커밋 지점에서 주기적으로 저장된다.
        // JobExecutionContext을 사용하면 Job 실행 중 서버가 다운이 되더라도 Job의 상태 정보를 유지할 수 있다.

        // # JobParameters
        // JobParameter를 설정하지 않고 동일한 이름의 Job을 중복 실행하면 NOOP 이라는 종료 코드가 기록되고 JOB이 실행되지 않는다.
        // JobParameters와 JobInstance는 1:1 관계이기 때문에 JobParameters는 Instance를 구분하는 기준이 된다.

        // # JobParameters와 Scope
        // JobParameters를 정상적으로 사용하기 위해서 JobScope나 StepScope를 설정해야 한다.
        // JobParameters는 Scope Bean의 생성 시점에서만 함께 생성될 수 있기 때문이라고 한다.
        // Scope Bean도 알아봐야겠다...
    }

    @Bean
    @JobScope // Job이 시작될 때 재생성됨
    public Step tutorialStep(
            @Value("#{jobParameters[datetimeLocalStr]}") String datetimeLocalStr,
            @Value("#{jobParameters[datetimeZonedStr]}") String datetimeZonedStr,
            @Value("#{jobParameters[datetimeUtcStr]}") String datetimeUtcStr,
            @Value("#{jobParameters[datetime]}") String datetime // Date 타입이지만, String 타입으로도 받아와지네
    ) {
        log.warn("JobScope tutorialStep() 시작");
        log.warn("datetimeUtcStr: {}", datetimeUtcStr);
        log.warn("datetime: {}", datetime);

        return stepBuilderFactory.get("tutorialStep")
                .tasklet(tutorialTasklet())
                .build();

        // 음.. JobScope로 Bean을 설정하니까 Job이 실행될 때마다 Bean이 새로 생성된다. 여기까진 이해가 가는데..
        // 왜 tutorialJob이 생성될 때 설정했던 tutorialStep()이 변경되어서 새로 생성된 tutorialStep이 실행되는지 이해가 안 간다.
        // 어떻게 tutorialJob이 기존에 물고있던 tutorialStep의 참조가 바뀌는건지..
        // JobScope로 설정된 bean은 참조시점에 Proxy로 생성되서 참조되나..? => 프록시 맞네
    }

    @Bean
    @StepScope
    public TutorialTasklet tutorialTasklet() {
        return new TutorialTasklet();
    }

    // job이 생성된 후의 출력
    /*
    2022-03-30 21:01:59.308  INFO 14780 --- [           main] com.zaxxer.hikari.HikariDataSource       : HikariPool-1 - Starting...
    2022-03-30 21:02:00.055  INFO 14780 --- [           main] com.zaxxer.hikari.HikariDataSource       : HikariPool-1 - Start completed.
    2022-03-30 21:02:00.155  INFO 14780 --- [           main] o.s.b.c.r.s.JobRepositoryFactoryBean     : No database type set, using meta data indicating: MYSQL
    2022-03-30 21:02:00.398  INFO 14780 --- [           main] o.s.b.c.l.support.SimpleJobLauncher      : No TaskExecutor has been set, defaulting to synchronous executor.
    2022-03-30 21:02:00.542  INFO 14780 --- [           main] org.quartz.impl.StdSchedulerFactory      : Using default implementation for ThreadExecutor
    2022-03-30 21:02:00.565  INFO 14780 --- [           main] org.quartz.core.SchedulerSignalerImpl    : Initialized Scheduler Signaller of type: class org.quartz.core.SchedulerSignalerImpl
    2022-03-30 21:02:00.565  INFO 14780 --- [           main] org.quartz.core.QuartzScheduler          : Quartz Scheduler v.2.3.2 created.
    2022-03-30 21:02:00.566  INFO 14780 --- [           main] org.quartz.simpl.RAMJobStore             : RAMJobStore initialized.
    2022-03-30 21:02:00.566  INFO 14780 --- [           main] org.quartz.core.QuartzScheduler          : org.quartz.Scheduler meta-data: Quartz Scheduler (v2.3.2) 'quartzScheduler' with instanceId 'NON_CLUSTERED'
        Scheduler class: 'org.quartz.core.QuartzScheduler' - running locally.
        NOT STARTED.
        Currently in standby mode.
        Number of jobs executed: 0 // 실행중인 jobs 없음
        Using thread pool 'org.quartz.simpl.SimpleThreadPool' - with 10 threads. // SimpleThreadPool로 Thread를 가져오나보다.
        Using job-store 'org.quartz.simpl.RAMJobStore' - which does not support persistence. and is not clustered.

    2022-03-30 21:02:00.566  INFO 14780 --- [           main] org.quartz.impl.StdSchedulerFactory      : Quartz scheduler 'quartzScheduler' initialized from an externally provided properties instance.
    2022-03-30 21:02:00.566  INFO 14780 --- [           main] org.quartz.impl.StdSchedulerFactory      : Quartz scheduler version: 2.3.2
    2022-03-30 21:02:00.567  INFO 14780 --- [           main] org.quartz.core.QuartzScheduler          : JobFactory set to: org.springframework.scheduling.quartz.SpringBeanJobFactory@6f6c6077
    2022-03-30 21:02:00.591  INFO 14780 --- [           main] o.s.s.quartz.SchedulerFactoryBean        : Starting Quartz Scheduler now
    2022-03-30 21:02:00.592  INFO 14780 --- [           main] org.quartz.core.QuartzScheduler          : Scheduler quartzScheduler_$_NON_CLUSTERED started.
    2022-03-30 21:02:00.598  INFO 14780 --- [           main] c.k.practice.batch.DemoApplication       : Started DemoApplication in 6.901 seconds (JVM running for 7.432)
    2022-03-30 21:02:00.600  INFO 14780 --- [           main] o.s.b.a.b.JobLauncherApplicationRunner   : Running default command line with: []
    2022-03-30 21:02:00.847  INFO 14780 --- [           main] o.s.b.c.l.support.SimpleJobLauncher      : Job: [SimpleJob: [name=tutorialJob]] launched with the following parameters: [{}] // SimpleJob이 실행됨

    // 첫 실행일 경우 Step이 실행되고, DB의 BATCH_JOB_EXECUTION를 보면 실행 성공 이력이 기록된다.
    2022-03-30 21:17:21.243  INFO 13112 --- [           main] o.s.batch.core.job.SimpleStepHandler     : Executing step: [tutorialStep]
    2022-03-30 21:17:25.133  INFO 13112 --- [           main] c.k.p.batch.tasklets.TutorialTasklet     : Tasklet 실행됨
    2022-03-30 21:17:25.157  INFO 13112 --- [           main] o.s.batch.core.step.AbstractStep         : Step: [tutorialStep] executed in 3s914ms

    // 첫 실행이 아닐 경우, 이미 실행된 이력이 있다면서 Step을 실행하지 않고, DB의 BATCH_JOB_EXECUTION를 보면 실행 실패 이력이 기록된다.
    // JobParameter를 별도로 설정하지 않아서 그렇다.
    // JobParameters와 JobInstance는 1:1 관계이기 때문에 JobParameters는 Instance를 구분하는 기준이 된다.
    2022-03-30 21:02:00.897  INFO 14780 --- [           main] o.s.batch.core.job.SimpleStepHandler     : Step already complete or not restartable, so no action to execute: StepExecution: id=1, version=3, name=tutorialStep, status=COMPLETED, exitStatus=COMPLETED, readCount=0, filterCount=0, writeCount=0 readSkipCount=0, writeSkipCount=0, processSkipCount=0, commitCount=1, rollbackCount=0, exitDescription=

    2022-03-30 21:02:00.955  INFO 14780 --- [           main] o.s.b.c.l.support.SimpleJobLauncher      : Job: [SimpleJob: [name=tutorialJob]] completed with the following parameters: [{}] and the following status: [COMPLETED] in 31ms  // Step을 모두 완료하고 SimpleJob이 종료됨. 걸린 시간도 나온다.
*/
}
