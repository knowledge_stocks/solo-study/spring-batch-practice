package com.kakaruu.practice.batch.schedulers;

import lombok.RequiredArgsConstructor;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionException;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

@Component
@RequiredArgsConstructor
public class TutorialScheduler {

    @Qualifier("conditionalFlowJob") private final Job job;
    private final JobLauncher jobLauncher;

    // 30초마다
    @Scheduled(cron = "*/10 * * * * *")
    public void executeJob() {
        try {
            JobParameters params = new JobParametersBuilder()
                    .addString("datetimeUtcStr",
                            ZonedDateTime.now().withZoneSameInstant(ZoneId.of("UTC")).toString()) // 2022-04-02T07:31:30.003391500Z[UTC]
                    .addDate("datetime", new Date()) // DB의 timezone이 utc이기 때문에 2022-04-02 오전 7:31:30.003000
                            .toJobParameters();
            // 개인적으로 ZonedDateTime을 놔두고 LocalDateTime을 왜 쓰는지 모르겠다.. Timezone이 중요한 프로그램이 별로 없나?

            jobLauncher.run(job, params);
        } catch (JobExecutionException e) {
            e.printStackTrace();
        }
    }
}
