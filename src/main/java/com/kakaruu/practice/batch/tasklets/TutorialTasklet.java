package com.kakaruu.practice.batch.tasklets;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Value;

@Slf4j
public class TutorialTasklet implements Tasklet {

    @Value("#{jobParameters[datetimeUtcStr]}")
    private String datetimeUtcStr;

    @Value("#{jobParameters[datetime]}")
    private String datetime;

    @Override
    public RepeatStatus execute(
            StepContribution contribution,
            ChunkContext chunkContext
    ) throws Exception {
        log.warn("Tasklet 실행됨");

//        StepExecution stepExecution = contribution.getStepExecution();
//        JobParameters jobParameters = stepExecution.getJobParameters();

//        log.warn("datetime: " + jobParameters.getDate("datetime"));
//        log.warn("datetimeUtcStr: " + jobParameters.getString("datetimeUtcStr"));
        log.warn("datetime: " + datetime);
        log.warn("datetimeUtcStr: " + datetimeUtcStr);

        return RepeatStatus.FINISHED;
        // 이 CONTINUABLE이 여러번 수행될 Tasklet을 설정할 때 사용하는 리턴값인 듯하다.
//        return RepeatStatus.CONTINUABLE;
    }
}
